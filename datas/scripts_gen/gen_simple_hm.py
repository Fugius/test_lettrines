from os import listdir
from os import makedirs
import os

import shutil
import random

import matplotlib.pyplot as plt
import numpy as np

path = "../heatmaps/"
classes = 3

for c in range(classes):
    files = listdir(path + str(c+1) + "/")
    os.mkdir("../raw_hm/" + str(c+1))
    for f in files:
        shutil.copy(path + str(c+1) + "/" + f + "/" + f, "../raw_hm/" + str(c+1) + "/" + f)

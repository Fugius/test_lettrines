from os import listdir
from os import makedirs
import os

import shutil
import random

import matplotlib.pyplot as plt
import numpy as np

#parameters
folds_dict = "5folds_dictionnary.txt"
simple_dict = "SimpleSplits_Dictionnary.txt"

#create base from dictionnaries
#simple split
c_path = ["../Gray_By_Classes/1/", "../Gray_By_Classes/2/", "../Gray_By_Classes/3/"]

src_level = "../simple/"
os.mkdir(src_level)

z_level = ""
o_level = ""

simple_file = open(simple_dict, "r")
lines = list(simple_file)

for line in lines:
    line = line[0:len(line)-1]
    if line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + z_level)

    elif line[0] == '-' :
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line, src_level + z_level + o_level + line)
        print(src_level + z_level + o_level + line)

simple_file.close()

#folds
src_level = "../" + "simple_folds/"
os.mkdir(src_level)

f_level = ""
z_level = ""
o_level = ""

folds_file = open(folds_dict, "r")
lines = list(folds_file)


for line in lines:
    line = line[0:len(line)-1]
    if line[0] == "$":
        f_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level)

    elif line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level)

    elif line[0] == '-' :
        print("LINE " + line)
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line, src_level + f_level + z_level + o_level + line)
        print(src_level + f_level + z_level + o_level + line)

folds_file.close()

#heatmaps
c_path = ["../" + "heatmaps/1/", "../" + "heatmaps/2/", "../" + "heatmaps/3/"]
src_level = "../" + "heatmaps_folds/"
os.mkdir(src_level)

f_level = ""
z_level = ""
o_level = ""

folds_file = open(folds_dict, "r")
lines = list(folds_file)


for line in lines:
    line = line[0:len(line)-1]
    if line[0] == "$":
        f_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level)

    elif line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level)

    elif line[0] == '-' :
        print("LINE " + line)
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line + "/" + line, src_level + f_level + z_level + o_level + line)
        print(src_level + f_level + z_level + o_level + line)

folds_file.close()

#mix channels
c_path = ["../" + "Mix_Channels_By_Classes/1/", "../" + "Mix_Channels_By_Classes/2/", "../" + "Mix_Channels_By_Classes/3/"]
src_level = "../" + "mix_folds/"
os.mkdir(src_level)

f_level = ""
z_level = ""
o_level = ""

folds_file = open(folds_dict, "r")
lines = list(folds_file)


for line in lines:
    line = line[0:len(line)-1]
    if line[0] == "$":
        f_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level)

    elif line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level)

    elif line[0] == '-' :
        print("LINE " + line)
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line, src_level + f_level + z_level + o_level + line)
        print(src_level + f_level + z_level + o_level + line)

folds_file.close()

#Bianry
c_path = ["../" + "Bin_By_Classes/1/", "../" + "Bin_By_Classes/2/", "../" + "Bin_By_Classes/3/"]
src_level = "../" + "Bin_folds/"
os.mkdir(src_level)

f_level = ""
z_level = ""
o_level = ""

folds_file = open(folds_dict, "r")
lines = list(folds_file)


for line in lines:
    line = line[0:len(line)-1]
    if line[0] == "$":
        f_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level)

    elif line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level)

    elif line[0] == '-' :
        print("LINE " + line)
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line, src_level + f_level + z_level + o_level + line)
        print(src_level + f_level + z_level + o_level + line)

folds_file.close()

from os import listdir
from os import makedirs
import os

import shutil
import random

import matplotlib.pyplot as plt
import numpy as np

import cv2
from skimage.io import imread, imsave
from skimage.filters import threshold_otsu

#parameters
path_unsorted = "../gray_and_rgb"
path = "../Gray_By_Classes"
path_heatmaps = "../heatmaps/"

class_numbers = 3

#Visualization of Binary image
def Bin2Gray(img):
  col, ligne = np.shape(img)
  imgRes = np.zeros((col, ligne))
  for i in range(len(img)):
    for j in range(len(img[i])):
      if (img[i][j] == True):
        imgRes[i][j] = 255
      else:
        imgRes[i][j] = 0
  return imgRes

#Gray to Binary image using otsu
def Gray2Otsu(img):
  Bin = threshold_otsu(img)
  imgBin = imgOR > Bin
  return imgBin

#Sort + convert raw data
makedirs(os.path.dirname("../Gray_By_Classes/1/"))
makedirs(os.path.dirname("../Gray_By_Classes/2/"))
makedirs(os.path.dirname("../Gray_By_Classes/3/"))

#transform + sort origin files into 3 classes folders
count = 0
files = listdir(path_unsorted)
for name in files:
    count += 1
    img = plt.imread(path_unsorted + "/" + name)
    if len(img.shape) == 2:
        img = np.stack((img, img, img), axis=2)
    if (int(name[1]) == 4):
        plt.imsave("../Gray_By_Classes/3/" + name, img)
    else:
        plt.imsave("../Gray_By_Classes/" + name[1] + "/" + name, img)
    print(str(count) + " images processed")

#Create new images on 3 channels, R = Gray, B = Binary, G = Heatmaps
os.mkdir("../Mix_Channels_By_Classes/")
for i in range(class_numbers):
    path = "../Gray_By_Classes/" + str(i+1) + "/"
    destination = "../Mix_Channels_By_Classes/" + str(i+1) + "/"

    os.mkdir("../Mix_Channels_By_Classes/" + str(i+1) + "/")

    img_files = listdir(path)

    for file in img_files:
        imgOR = imread(path + file, as_gray = True)
        imgOR *= 255
        imgBin = Gray2Otsu(imgOR)
        imgBinG = Bin2Gray(imgBin)
        imgHM = imread(path_heatmaps + str(i+1) + "/" + file + "/" + file, as_gray = True)
        imgHM *= 255

        img = cv2.merge((imgOR, imgBinG, imgHM))

        imsave(destination + file, img)
        print("Created " + destination + file)

#Create Binary images
os.mkdir("../Bin_By_Classes/")
for i in range(class_numbers):
    path = "../Gray_By_Classes/" + str(i+1) + "/"
    destination = "../Bin_By_Classes/" + str(i+1) + "/"

    os.mkdir(destination)

    img_files = listdir(path)

    for file in img_files:
        imgOR = imread(path + file, as_gray = True)
        imgBin = Gray2Otsu(imgOR)
        imgBinG = Bin2Gray(imgBin)

        img = cv2.merge((imgBinG, imgBinG, imgBinG))

        imsave(destination + file, img)
        print("Created " + destination + file)

from os import listdir
from os import makedirs
import os

import shutil
import random

import matplotlib.pyplot as plt
import numpy as np

"""
Code generates 5 folds 
Organizes data from /gray_and_rgb to sorted classes /Gray_By_Classes
/gray_and_rgb : unsorted original data
/Gray_By_Classes : sorted original data
Transforms data from /Gray_By_Classes to 3-Channels images organized in  /Mix_Channels_By_Classes
"""

#change this path according to your device
pathDev = "/content/gdrive/My Drive/Colab Notebooks/projetTER/binary"

#parameters
#files that contain how the data will be separeted
simple_dict = pathDev+"/SimpleSplits_Dictionnary.txt"
folds_dict = pathDev+"/5folds_dictionnary.txt"

#k-folds on mix channels
c_path = [pathDev+"/" + "Binary_By_Classes/1/", pathDev+"/" + "Binary_By_Classes/2/", pathDev+"/" + "Binary_By_Classes/3/"]

c_path = [pathDev+"/Binary_By_Classes/1/", pathDev+"/Binary_By_Classes/2/", pathDev+"/Binary_By_Classes/3/"]

src_level = pathDev+"/simple/"
os.mkdir(src_level)

z_level = ""
o_level = ""

simple_file = open(simple_dict, "r")
lines = list(simple_file)

for line in lines:
    line = line[0:len(line)-1]
    if line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + z_level)

    elif line[0] == '-' :
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line, src_level + z_level + o_level + line)
        print(src_level + z_level + o_level + line)

simple_file.close()

#folds
src_level = pathDev + "/" + "simple_folds/"
os.mkdir(src_level)

f_level = ""
z_level = ""
o_level = ""

folds_file = open(folds_dict, "r")
lines = list(folds_file)


for line in lines:
    line = line[0:len(line)-1]
    if line[0] == "$":
        f_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level)

    elif line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level)

    elif line[0] == '-' :
        print("LINE " + line)
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line, src_level + f_level + z_level + o_level + line)
        print(src_level + f_level + z_level + o_level + line)

folds_file.close()

src_level = pathDev+"/" + "binary_folds/"
os.mkdir(src_level)

f_level = ""
z_level = ""
o_level = ""

folds_file = open(folds_dict, "r")
lines = list(folds_file)


for line in lines:
    line = line[0:len(line)-1]
    if line[0] == "$":
        f_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level)

    elif line[0] == '#' :
        z_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level)

    elif line[0] == '-' :
        print("LINE " + line)
        o_level = line[1:len(line)] + "/"
        os.mkdir(src_level + f_level + z_level + o_level)

    else:
        shutil.copy(c_path[int(o_level[0:len(o_level)-1]) - 1] + line, src_level + f_level + z_level + o_level + line)
        print(src_level + f_level + z_level + o_level + line)

folds_file.close()

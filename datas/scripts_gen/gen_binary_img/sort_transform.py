from os import listdir
from os import makedirs
import os

import shutil
import random

import matplotlib.pyplot as plt
import numpy as np

import cv2
from skimage.io import imread, imsave
from skimage.filters import threshold_otsu

"""
Code that normalizes and transforms the images
Organizes data from /gray_and_rgb to sorted classes /Gray_By_Classes
/gray_and_rgb : unsorted original data
/Gray_By_Classes : sorted original data
Transforms data from /Gray_By_Classes to 3-Channels images organized in  /Mix_Channels_By_Classes
"""

#change this path according to your device
pathDev = "/content/gdrive/My Drive/Colab Notebooks/projetTER/binary"

#parameters
path = pathDev + "/Gray_By_Classes"

class_numbers = 3

def Gray2Otsu(img):
  """
  Transforms a grayscale image to a binary image

  Parameters
  ----------
  img : grayscale image

  Returns
  -------
  imgRes : binary image transformed via the OTSU threshold
  """
  Bin = threshold_otsu(img)
  imgBin = imgOR > Bin
  return imgBin

def Bin2Gray(img):
  """
  Transforms a binary image to a grayscale image since a binary image cannot be
  used as a channel 

  Parameters
  ----------
  img : binary image

  Returns
  -------
  imgRes : binary image transformed to grayscale
  """
  col, ligne = np.shape(img)
  #initializing an empty 0 image
  imgRes = np.zeros((col, ligne))
  #iterating through the image
  for i in range(len(img)):
    for j in range(len(img[i])):
      #
      if (img[i][j] == True):
        imgRes[i][j] = 255
      else:
        imgRes[i][j] = 0
  return imgRes

#Sort + convert raw data
#Creation of the classes folders
class_numbers = 3
#Create new images on 3 channels, R = Gray, B = Binary, G = Heatmaps
os.mkdir(pathDev+"/Binary_By_Classes/")
for i in range(class_numbers):
    path = pathDev + "/Gray_By_Classes/" + str(i+1) + "/"
    destination = pathDev + "/Binary_By_Classes/" + str(i+1) + "/"

    os.mkdir(pathDev + "/Binary_By_Classes/" + str(i+1) + "/")

    img_files = listdir(path)

    for file in img_files:
        imgOR = imread(path + file, as_gray = True)
        imgBin = Gray2Otsu(imgOR)
        imgBinG = Bin2Gray(imgBin)

        imsave(destination + file, imgBinG)
        print("Created " + destination + file)
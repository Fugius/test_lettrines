from os import listdir
from os import makedirs
import os

import shutil
import random

import matplotlib.pyplot as plt
import numpy as np

#parameters
n_folds = 5
class_number = 3
path_origin = "../Gray_By_Classes/"

#simple split result
simple_train = []
simple_test = []

#Folds result
folds = []
for i in range(n_folds):
    folds.append([])

#Acquire all images
images_by_class = []
for i in range(class_number):
    images_by_class.append(listdir(path_origin + str(i+1)))

#Simple split 75/25
for class_content in images_by_class:
    split_size = int(len(class_content)*0.25)

    test_split = random.sample(class_content, split_size)
    simple_test.append(test_split)

    train_split = []
    for img in class_content:
        if img not in test_split: train_split.append(img)

    simple_train.append(train_split)

#KFolds splits
for k in range(n_folds):
    f_train = []
    f_test = []
    f_eval = []
    for c in range(class_number):
        split_size = int(len(class_content)/n_folds)

        test_split = images_by_class[c][k*split_size:k*split_size+split_size]
        r = images_by_class[c][0:k*split_size] + images_by_class[c][k*split_size+split_size:len(images_by_class[c])]

        eval_split = random.sample(r, split_size)

        train_split = []
        for img in r:
            if img not in eval_split: train_split.append(img)

        f_train.append(train_split)
        f_test.append(test_split)
        f_eval.append(eval_split)
    folds[k].append(f_train)
    folds[k].append(f_test)
    folds[k].append(f_eval)

#Write Siple splits in dictionnary file
simple_file = open("SimpleSplits_Dictionnary.txt", "w")

simple_file.write("#TRAIN\n")
for c in range(len(simple_train)):
    simple_file.write("-" + str(c+1) + "\n")
    for file in simple_train[c]:
        simple_file.write(file + "\n")

simple_file.write("#TEST\n")
for c in range(len(simple_test)):
    simple_file.write("-" + str(c+1) + "\n")
    for file in simple_test[c]:
        simple_file.write(file + "\n")

simple_file.close()

#Write folds dictionnary
kfolds_file = open(str(n_folds) + "folds_dictionnary.txt", "w")

for i in range(len(folds)):
    kfolds_file.write("$k" + str(i) + "\n")

    kfolds_file.write("#TRAIN\n")
    for c in range(len(folds[i][0])):
        kfolds_file.write("-" + str(c+1) + "\n")
        for file in folds[i][0][c]:
            kfolds_file.write(file + "\n")

    kfolds_file.write("#TEST\n")
    for c in range(len(folds[i][1])):
        kfolds_file.write("-" + str(c+1) + "\n")
        for file in folds[i][1][c]:
            kfolds_file.write(file + "\n")

    kfolds_file.write("#EVAL\n")
    for c in range(len(folds[i][2])):
        kfolds_file.write("-" + str(c+1) + "\n")
        for file in folds[i][2][c]:
            kfolds_file.write(file + "\n")

kfolds_file.close()

# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 23:41:54 2020

@author: dball
"""

from skimage import util
from skimage.io import imread, imshow
from skimage.color import gray2rgb
from skimage.filters import threshold_otsu
import enlacement

import os

#path des fichiers
count = 1
for i in range(3):

    path = "../" + "../datas/raw_data/" + str(i+1) + "/"
    #sauvegarde des noms des images du fichier dans une liste
    listImg1 = os.listdir(path)
    #listImg1
    #len(listImg1)
    #parcourir le fichier

    #boucler la liste et creer les vars

    for img in listImg1:

        #definir le path de l'image
        #path cree a partir du nom de l'image traite
        pathImg = path+img
        #pathImg

        #lecture de l'image
        imgA = imread(pathImg, as_gray=True)

        #binarisation otsu image A
        BinA = threshold_otsu(imgA)
        imgBinA = imgA > BinA
        #imshow(imgBinA)

        #creation de l'image B inverse de A
        imgBinB = util.invert(imgBinA)
        #imshow(imgBinB)

        #interlacement
        #sauvegarde comme Pickel File
        i_ab, e_ab, e_ba = enlacement.interlacement(imgBinA, imgBinB)

        #heatmaps
        #sauvegarde comme Images
        #HM finale = hm_ab + hm_ba
        hm_ab = enlacement.enlacement_heatmaps(imgBinA, imgBinB)
        hm_ba = enlacement.enlacement_heatmaps(imgBinB, imgBinA)

        #sauvegarde data

        #creer path selon le nom du fichier pour sauv les resultats
        pathRes = "../" + "../datas/heatmaps/"+str(i+1)+"/"+img
        #creation fichier qui va contenir les resultats
        #nom du fichier = nom image
        makedirs(os.path.dirname(pathRes))

        #sauvegarde de tous les resultats
        enlacement.dump(pathRes, imgBinA, imgBinB,e_ab, e_ba, i_ab, hm_ab, hm_ba)

        #experimentale
        #delete les var de la memoire pour liberer d'espace
        del imgA, BinA, imgBinA, imgBinB
        del pathImg, pathRes
        del e_ab, e_ba, i_ab, hm_ab, hm_ba

        print(count, "/", 721)
        count += 1
    #########

#sur = enlacement.surrounding(e_ab) # 0.999999987

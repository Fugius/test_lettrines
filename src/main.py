import LM

md = LM.LearningMachine(3, "../datas/simple_folds/k0/TEST", "../datas/simple_folds/k0/TRAIN")
md.train(50)

md.set_testing_datas("../datas/simple_folds/k0/EVAL")
acc = md.accuracy()

print(acc)

from LM import LearningMachine

import time
import copy

from os import listdir
from os import makedirs
import os

import json

def kfold(fold_path, n_folds, results_path):
    models_acc = []
    models_loss = []

    #for each fold
    count = 0
    for i in range(n_folds):
        print('Fold ' + str(i) + " : ")

        #setup the model for the current fold
        lm = LearningMachine(train_path=fold_path+'/k'+str(i)+'/TRAIN', test_path=fold_path+'/k'+str(i)+'/TEST', num_classes=3, lr=0.001, momentum=0.9)

        #train
        lm.train(100)

        #get eval accuracy
        lm.set_testing_datas(fold_path+'/k'+str(i)+'/EVAL')
        accuracy = lm.accuracy()

        print("Eval accuracy : ", accuracy)

        #save results
        models_acc.append(accuracy)
        models_loss.append(lm.getLoss())

        lm.save(results_path + "/" + str(count) + ".pth")
        count += 1

    #get average accuracy
    mean_acc = 0

    for a in models_acc:
        mean_acc += a
    mean_acc /= len(models_acc)

    #save models
    count = 0
    for a in models_loss:
        f = open(results_path + "/" + str(count) + "_loss.json", 'w')
        f.write(json.dumps(a))
        f.close()
        count += 1

    print('Models saved in : ', fold_path)
    print('Average accuracy : ' , mean_acc , '\nExit success')

    return mean_acc

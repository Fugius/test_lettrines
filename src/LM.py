"""
Class LearningMachine: Wrapper that load datasets, import a Squeezenet_1.0 pretrained model and finetune it

Constructor: test_path, train path: path to data folder orginized by classes
             lr : learning rate for optimizer
             momentum : momentum for SGD https://en.wikipedia.org/wiki/Stochastic_gradient_descent#Momentum

train: train the model until the accuracy on testing dataset decreases

accuracy: find model accuracy on testing set (private)

save: save to *path* in the form : dict['model':model_dict]

load: load a model stored in *path*

set_training_datas, set_testing_datas : same format as test_path and train_path in constructor
"""
#imports
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import torchvision
import torchvision.transforms as transform

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

import copy
import sys
import time

class LearningMachine():
    def __init__(self, num_classes, test_path='', train_path='', lr=0.0001, momentum=0.9, num_workers=3): #init datas and model
        #Configure GPU
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        print("Using device : ", self.device)


        if (test_path != '' and train_path != ''):
            #create datas modifiers (normalization)
            transforms = transform.Compose([transform.Resize((224, 244)), transform.ToTensor(), transform.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])

            #trainnig datas
            train_data = torchvision.datasets.ImageFolder(train_path, transform=transforms)
            self.train_loader = torch.utils.data.DataLoader(train_data, batch_size=10, shuffle=True, num_workers=num_workers)

            #test datas
            test_data = torchvision.datasets.ImageFolder(test_path, transform=transforms)
            self.test_loader = torch.utils.data.DataLoader(test_data, batch_size=10, shuffle=True, num_workers=num_workers)

        #load + finetune pretrained model
        self.model = torch.hub.load('pytorch/vision:v0.5.0', 'squeezenet1_0', pretrained=True)
        self.model.classifier = nn.Sequential(
            nn.Dropout(p=0.5),
            nn.Conv2d(512, num_classes, kernel_size=1),
            nn.ReLU(inplace=True),
            nn.AvgPool2d(13)
        )
        self.model.forward = lambda x: self.model.classifier(self.model.features(x)).view(x.size(0), num_classes)
        self.model.to(self.device)

        #Setup loss and entropy function
        self.lf = nn.CrossEntropyLoss()
        self.optimizer = optim.SGD(self.model.parameters(), lr, momentum)

        self.loss = []

    def accuracy(self):
        acc = 0
        total = 0
        correct = 0

        with torch.no_grad():
            self.model.eval()
            for batch in self.test_loader:
                imgs, labels = batch[0].to(self.device), batch[1].to(self.device)

                output = self.model(imgs)
                predicted = output.data.max(1)[1]

                total += labels.size(0)
                correct += (predicted == labels).sum().item()

            acc = correct/total
        return acc

    def __call__(self, single_path):
        transforms = transform.Compose([transform.Resize((224, 244)), transform.ToTensor(), transform.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
        img = Image.open(single_path).convert('RGB')
        tensor = transforms(img).unsqueeze_(0).to(self.device)

        output = self.model(tensor)
        predicted = output.data.max(1)[1]

        return predicted.item()

    def train(self, max_epochs):
        print("Start training\n")
        best_accs = {}
        last_acc = 0


        for i in range(max_epochs):
            avg_loss = 0
            n_data = 0
            last_model = copy.deepcopy(self.model.state_dict())

            print("Batch number : ", end='')

            #for each batch
            for batch in self.train_loader:
                self.model.train()
                imgs, labels = batch[0].to(self.device), batch[1].to(self.device)

                output = self.model(imgs)
                loss = self.lf(output, labels)

                self.loss.append(loss.item())

                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                avg_loss += loss
                n_data += 1

                if n_data % 10 == 0:
                    sys.stdout.flush()
                    print(n_data, end=' ')
                    #time.sleep(2)

            avg_loss /= n_data
            accuracy = self.accuracy()
            print("\nEpoch : ", i, " | training loss : ", avg_loss.item(), " | Accuracy : ", accuracy)

            if accuracy < last_acc:
                print("\nModel OverFitting, stop learning, best accuracy : ", sorted(best_accs.keys())[len(best_accs)-1] if len(best_accs) > 0 else last_acc)
                if len(best_accs) > 4:
                    break
                else:
                    best_accs[last_acc]= last_model
                    print(len(best_accs))
            else:
                last_acc = accuracy
                last_model = copy.deepcopy(self.model.state_dict())

        if len(best_accs) > 0:
            md = best_accs[sorted(best_accs.keys())[len(best_accs)-1]]
            self.model.load_state_dict(md)

    def getLoss(self):
        return self.loss

    def load(self, path):
        saved_model = torch.load(path)
        self.model.load_state_dict(saved_model["model"])
        #acc = self.accuracy()
        print('Model loaded : ', path)
        #print('Model accuracy on current dataset : ', acc)

    def save(self, path):
        torch.save({
            'model': self.model.state_dict()
        }, path)
        print('Model saved in : ', path)

    def set_training_datas(self, path, num_workers=3):
        transforms = transform.Compose([transform.Resize((224, 244)), transform.ToTensor(), transform.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
        train_data = torchvision.datasets.ImageFolder(path, transform=transforms)
        self.train_loader = torch.utils.data.DataLoader(train_data, batch_size=10, shuffle=True, num_workers=num_workers)
        print('Training datas loaded : ', path)

    def set_testing_datas(self, path, num_workers=3):
        transforms = transform.Compose([transform.Resize((224, 244)), transform.ToTensor(), transform.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])])
        test_data = torchvision.datasets.ImageFolder(path, transform=transforms)
        self.test_loader = torch.utils.data.DataLoader(test_data, batch_size=10, shuffle=True, num_workers=num_workers)
        print('Testing datas loaded : ', path)
